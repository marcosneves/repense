module ApplicationHelper
  def new_btn(path)
    content_tag('div', class: 'pull-right') do
      concat(link_to(path, class: 'btn btn-lg btn-success') do
        concat content_tag('span', '', class: 'glyphicon glyphicon-plus')
      end)
    end
  end

  def edit_btn(model)
    link_to [:edit, model], class: 'btn btn-xs btn-info' do
      concat content_tag('span', '', class: 'glyphicon glyphicon-edit')
    end
  end

  def destroy_btn(model)
    link_to [:edit, model], method: :delete, class: 'btn btn-xs btn-danger', data: {confirm: 'Tem Certeza?'} do
      concat content_tag('span', '', class: 'glyphicon glyphicon-trash')
    end
  end
end
