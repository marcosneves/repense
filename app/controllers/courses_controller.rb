class CoursesController < InheritedResources::Base
  include InheritedResourcesDefaults

  private

  def course_params
    params.require(:course).permit(:name, :description, :status)
  end
end
