class ClassroomsController < InheritedResources::Base
  include InheritedResourcesDefaults

  private

  def classroom_params
    params.require(:classroom).permit(:course_id, :student_id)
  end
end
