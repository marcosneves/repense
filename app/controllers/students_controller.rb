class StudentsController < InheritedResources::Base
  include InheritedResourcesDefaults

  private

  def student_params
    params.require(:student).permit(:name, :register_number, :status)
  end
end
