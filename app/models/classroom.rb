class Classroom < ActiveRecord::Base
  belongs_to :student
  belongs_to :course
  validates :student_id, :course_id, presence: true
  validates_uniqueness_of :course_id, scope: :student_id

  private

  def timestamp_attributes_for_create
    [:entry_at]
  end
end
