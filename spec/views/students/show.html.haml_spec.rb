require 'rails_helper'

RSpec.describe "students/show", type: :view do
  before(:each) do
    @student = assign :student, create(:student)
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/Register number/)
    expect(rendered).to match(/1/)
  end
end
