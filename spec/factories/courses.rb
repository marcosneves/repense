FactoryGirl.define do
  factory :course do
    name 'Foo Bar'
    description 'Foo bar description'
    status 1
  end
end
