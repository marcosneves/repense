FactoryGirl.define do
  factory :student do
    name 'John Doe'
    register_number '1234567890'
    status 1
  end
end
