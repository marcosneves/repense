class CreateClassrooms < ActiveRecord::Migration
  def change
    create_table :classrooms do |t|
      t.references :course
      t.references :student

      t.index [:course_id, :student_id], unique: true
      t.index [:student_id, :course_id], unique: true

      t.datetime :entry_at, null: false
    end
  end
end
